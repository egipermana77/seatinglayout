var setting = {
	baris: 5,
	kolom: 4,
	space: 2,
	barisCssPrefix: 'row-',
	kolomCssPrefix: 'col-',
	lebarKursi: 35,
	tinggiKursi: 35,
	kursiCss: 'seat',
	kursiTerboking: 'selectedSeat',
	kursiTerpilih: 'selectingSeat'
}

var init = function(reservedSeat)
{
	var str = [], kursiNo, className;
	for(i = 0; i < setting.baris; i++)
	{
		for(j = 0; j < setting.kolom; j++)
		{
			kursiNo = (j + i * setting.kolom + 1);
			className = setting.kursiCss + ' ' + setting.barisCssPrefix + i.toString() + ' ' + setting.kolomCssPrefix + j.toString();
			//cek untuk kursi telah terbooking
			if($.isArray(reservedSeat) && $.inArray(kursiNo, reservedSeat) != -1)
			{
				className += ' ' + setting.kursiTerboking;
			}
			str.push(
				'<div class="' + className + '"' + 'style="top: ' + (i * setting.tinggiKursi).toString() 
				+ 'px;left:' + (j * setting.lebarKursi).toString() + 'px;">' +
				'<a title="' + kursiNo + '">' + kursiNo + '</a>' + '</div>'
			);
		}
	}
	$('#place').html(str.join(''));
}

var init2 = function(reservedSeat, count)
{
	var str = [], kursiNo, className;
	var subsBaris = Math.floor(count / setting.kolom);
	
	kursiNo = 1;

	for(i = 0; i < subsBaris + 1; i++)
	{
		for(j = 0; j < setting.kolom; j++)
		{
			className = setting.kursiCss + ' ' + setting.barisCssPrefix + i.toString() + ' ' + setting.kolomCssPrefix + j.toString();
			//cek untuk kursi telah terbooking
			if($.isArray(reservedSeat) && $.inArray(kursiNo, reservedSeat) != -1)
			{
				className += ' ' + setting.kursiTerboking;
			}
			
			if(j === 1 && i !== subsBaris ){
				str.push('<div class="space" style="top: '+(i * setting.tinggiKursi).toString()+'px;left: '+(j * setting.lebarKursi).toString()+'px; "></div>');
				kursiNo -= 1;
			}else{
				str.push(
					'<div class="' + className + '"' + 'style="top: ' + (i * setting.tinggiKursi).toString() 
					+ 'px;left:' + (j * setting.lebarKursi).toString() + 'px;">' +
					'<a title="' + kursiNo + '">' + kursiNo + '</a>' + '</div>'
				);
			}
		kursiNo++;
		}
	}
	$('#layout').html(str);
}



var diboking = [1,3,7];
init(diboking);
init2(diboking,16);

$('.' + setting.kursiCss).click(function(e){
	if($(this).hasClass(setting.kursiTerboking))
	{
		alert('Maaf, kursi telah dibooking');
	}else{
		$(this).toggleClass(setting.kursiTerpilih);
	}
});

//menampilkan semua kursi terpilih
$('#btnShow').click(function(){
	var str = [];
	$.each($('#place div.' + setting.kursiTerboking + ' a, #place div.' + setting.kursiTerpilih + ' a'), function(index,value){
		str.push($(this).attr('title'));
	});
	alert(str.join(','));
});

//menampilkan kursi yang dipilih user
$('#btnShowNew').click(function(){
	var str = [], item;
	$.each($('#place div.' + setting.kursiTerpilih + ' a'), function(index, value){
		item = $(this).attr('title');
		str.push(item);
	});
	alert(str.join(','));
});